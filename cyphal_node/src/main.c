/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "canard.h"
#include <assert.h>
#include <stdio.h>
#include <zephyr/drivers/can.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/kernel.h>
#include <zephyr/nunavut_out/uavcan/node/Heartbeat_1_0.h>
#include <zephyr/sys/time_units.h>

/* The devicetree node identifier for the "led0" alias. */
#define LED0_NODE              DT_ALIAS(led0)
#define NODE_ID                42
#define HEARTBEAT_PERIOD_MS    1000
#define NODE_TX_QUEUE_CAPACITY 50
#define LED_FLASH_INTERVAL_MS  50  // in milliseconds

const struct device* const       can_dev = DEVICE_DT_GET(DT_CHOSEN(zephyr_canbus));
static const struct gpio_dt_spec led     = GPIO_DT_SPEC_GET(LED0_NODE, gpios);

struct k_mutex lock;                              // Zephyr mutex
struct k_timer heartbeat_timer, led_flash_timer;  // Zephyr timer

CanardInstance canard;
CanardTxQueue  queue;

// ----------------------- PRIVATE FUNCTION DEFINITIONS -----------------------

static void send_heartbeat(struct k_timer* timer_id);

static void can_controller_init(void);

static void send_over_fdcan(void);

static void configure_led(void);

static void flash_led(void);

static void flash_led_timer_callback(struct k_timer* timer_id);

// ----------------------- PRIVATE FUNCTION DEFINITIONS -----------------------

// wrapper functions
static void* canard_malloc(CanardInstance* ins __unused, size_t amount) { return k_malloc(amount); }

static void canard_free(CanardInstance* ins __unused, void* pointer) { k_free(pointer); }

static void node_init() {
    canard         = canardInit(canard_malloc, canard_free);
    canard.node_id = NODE_ID;
    queue          = canardTxInit(NODE_TX_QUEUE_CAPACITY, CANARD_MTU_CAN_FD);

    // create led flash timer
    k_timer_init(&led_flash_timer, flash_led_timer_callback, NULL);

    // create an heartbeat timer
    k_timer_init(&heartbeat_timer, send_heartbeat, NULL);
    // start a periodic timer that expires after every one second
    k_timer_start(&heartbeat_timer, K_MSEC(HEARTBEAT_PERIOD_MS), K_MSEC(HEARTBEAT_PERIOD_MS));
}

static void can_controller_init(void) {

    if (!device_is_ready(can_dev)) {
        printk("CAN: Device %s not ready.\n", can_dev->name);
    }

    can_set_mode(can_dev, CAN_MODE_NORMAL | CAN_MODE_FD);
    can_start(can_dev);
}

static void configure_led(void) {
    if (!gpio_is_ready_dt(&led)) {
        printk("LED: Heartbeat flashing led is not ready!\n");
    }

    gpio_pin_configure_dt(&led, GPIO_OUTPUT_ACTIVE);

    gpio_pin_set_dt(&led, 0);  // turn off led
}

static void flash_led(void) {
    k_timer_start(&led_flash_timer, K_MSEC(LED_FLASH_INTERVAL_MS), K_NO_WAIT);
    gpio_pin_set_dt(&led, 1);  // set led
}

static void flash_led_timer_callback(struct k_timer* timer_id) {
    gpio_pin_set_dt(&led, 0);  // reset led
}

// ----------------------- PUBLIC FUNCTION DEFINITIONS -----------------------

int main(void) {
    int ret;

    configure_led();
    can_controller_init();
    node_init();

    while (1) {
        // do nothing and sleep
        k_sleep(K_FOREVER);
    }
    return 0;
}

static void send_heartbeat(struct k_timer* timer_id) {

    const struct __packed Heartbeat10 {
        uint32_t uptime;
        uint8_t  health;
        uint8_t  mode;
        uint8_t  vssc;
    } hb = {.uptime = k_uptime_get_32() / 1000,          // in seconds
            .health = uavcan_node_Health_1_0_NOMINAL,    // lets assume
            .mode   = uavcan_node_Mode_1_0_OPERATIONAL,  // lets assume
            .vssc   = 0};

    static_assert(sizeof(hb) == uavcan_node_Heartbeat_1_0_SERIALIZATION_BUFFER_SIZE_BYTES_, "Something's wrong");

    static uint8_t transfer_id = 0;  // Must be static or heap-allocated to retain
                                     // state between calls.

    const CanardTransferMetadata transfer_metadata = {.priority       = CanardPriorityNominal,
                                                      .transfer_kind  = CanardTransferKindMessage,
                                                      .port_id        = uavcan_node_Heartbeat_1_0_FIXED_PORT_ID_,
                                                      .remote_node_id = CANARD_NODE_ID_UNSET,
                                                      .transfer_id    = transfer_id++};

    int32_t result = canardTxPush(&queue, &canard, 0, &transfer_metadata,
                                  uavcan_node_Heartbeat_1_0_SERIALIZATION_BUFFER_SIZE_BYTES_, (uint8_t*) &hb);
    if (result < 0) {
        // An error has occurred: either an argument is invalid, the TX queue is
        // full, or we've run out of memory. It is possible to statically prove that
        // an out-of-memory will never occur for a given application if the heap is
        // sized correctly; for background, refer to the Robson's Proof and the
        // documentation for O1Heap.
    }

    // call CAN send function
    send_over_fdcan();
}

static void tx_callback(const struct device* dev, int error, void* user_data) {
    char* sender = (char*) user_data;

    if (error != 0) {
        printk("sending failed [%d]\n", error);
    }
}

static void send_over_fdcan() {

    for (const CanardTxQueueItem* ti = NULL; (ti = canardTxPeek(&queue)) != NULL;)  // Peek at the top of the queue.
    {
        struct can_frame frame = {.flags = CAN_FRAME_IDE};

        bytecpy(&frame.data[0], ti->frame.payload, ti->frame.payload_size);
        frame.id  = ti->frame.extended_can_id;
        frame.dlc = can_bytes_to_dlc(ti->frame.payload_size);

        can_send(can_dev, &frame, K_MSEC(100), tx_callback, "Sender 1");

        flash_led();

        // After the frame is transmitted or if it has timed out while waiting, pop
        // it from the queue and deallocate:
        canard.memory_free(&canard, canardTxPop(&queue, ti));
    }
}
