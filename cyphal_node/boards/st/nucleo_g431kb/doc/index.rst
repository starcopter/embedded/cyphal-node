.. _nucleo_g431kb_board:

ST Nucleo G431KB
################

Overview
********

The STM32 Nucleo-32 board provides an affordable and flexible way for users to try out new concepts and build
prototypes by choosing from the various combinations of performance and power consumption features, provided by
the STM32 microcontroller.

The Arduino™ Nano V3 connectivity support allows the easy expansion of the functionality of the STM32 Nucleo
open development platform with a wide choice of specialized shields.

The STM32 Nucleo-32 board does not require any separate probe as it integrates the ST-LINK debugger/programmer.

.. image:: img/nucleo_g431kb.jpg
  :align: center
  :alt: Nucleo G431KB

More information about the board can be found at the `Nucleo G431KB website`_.

Hardware
********

The Nucleo G431KB provides the following hardware components:

- STM32G4 microcontroller in 32-pin package featuring 128 KB of Flash memory and 32 KB of SRAM
- ARM |reg| 32-bit Cortex |reg| -M4 CPU with FPU, frequency up to 170 MHz
- On-board STLINK-V3E
- Arduino™ Nano V3 compatible pinout

More information about STM32G431KB can be found here:

- `STM32G431KB on www.st.com`_
- `STM32G4 reference manual`_

Supported Features
==================

The Zephyr nucleo_g431kb board configuration supports the following hardware features:

+-----------+------------+-------------------------------------+
| Interface | Controller | Driver/Component                    |
+===========+============+=====================================+
| NVIC      | on-chip    | nested vector interrupt controller  |
+-----------+------------+-------------------------------------+
| UART      | on-chip    | serial port-polling;                |
|           |            | serial port-interrupt               |
+-----------+------------+-------------------------------------+
| PINMUX    | on-chip    | pinmux                              |
+-----------+------------+-------------------------------------+
| GPIO      | on-chip    | gpio                                |
+-----------+------------+-------------------------------------+
| I2C       | on-chip    | i2c                                 |
+-----------+------------+-------------------------------------+
| PWM       | on-chip    | pwm                                 |
+-----------+------------+-------------------------------------+
| DAC       | on-chip    | dac                                 |
+-----------+------------+-------------------------------------+
| SPI       | on-chip    | spi                                 |
+-----------+------------+-------------------------------------+
| RNG       | on-chip    | rng                                 |
+-----------+------------+-------------------------------------+

Other hardware features are not yet supported on this Zephyr port.

The default configuration can be found in the defconfig file:
``boards/arm/nucleo_g431kb/nucleo_g431kb_defconfig``


Connections and IOs
===================

Nucleo G431KB Board has 6 GPIO controllers. These controllers are responsible for pin muxing,
input/output, pull-up, etc.

For more details please refer to `STM32G4 Nucleo-64 board User Manual`_.

Default Zephyr Peripheral Mapping:
----------------------------------

.. rst-class:: rst-columns

- UART_1_TX : PC4
- UART_1_RX : PC5
- LPUART_1_TX : PA2
- LPUART_1_RX : PA3
- I2C_1_SCL : PB8
- I2C_1_SDA : PB9
- SPI_1_NSS : PB6
- SPI_1_SCK : PA5
- SPI_1_MISO : PA6
- SPI_1_MOSI : PA7
- SPI_2_NSS : PB12
- SPI_2_SCK : PB13
- SPI_2_MISO : PB14
- SPI_2_MOSI : PB15
- SPI_3_NSS : PA15
- SPI_3_SCK : PC10
- SPI_3_MISO : PC11
- SPI_3_MOSI : PC12
- PWM_3_CH1 : PB4
- USER_PB : PC13
- LD2 : PA5
- DAC1_OUT1 : PA4

System Clock
------------

Nucleo G431KB System Clock could be driven by internal or external oscillator,
as well as main PLL clock. By default System clock is driven by PLL clock at 150MHz,
driven by 16MHz high speed internal oscillator. The clock can be boosted to 170MHz if boost mode
is selected.

Serial Port
-----------

Nucleo G431KB board has 3 U(S)ARTs and one LPUART. The Zephyr console output is assigned to LPUART1.
Default settings are 115200 8N1.

Please note that LPUART1 baudrate is limited to 9600 if the MCU is clocked by LSE (32.768 kHz) in
low power mode.

Programming and Debugging
*************************

Applications for the ``nucleo_g431kb`` board configuration can be built and
flashed in the usual way (see :ref:`build_an_application` and
:ref:`application_run` for more details).

Flashing
========

Nucleo G431KB board includes an ST-LINK/V3E embedded debug tool interface.

This interface is not yet supported by the openocd version included in the Zephyr SDK.

Instead, support can be enabled on pyocd by adding "pack" support with
the following pyocd command:

.. code-block:: console

   $ pyocd pack --update
   $ pyocd pack --install stm32g431kb

Note:
To manually enable the openocd interface, You can still update, compile and install
a 'local' openocd from the official openocd repo http://openocd.zylin.com .
Then run the following openocd command where the '/usr/local/bin/openocd'is your path
for the freshly installed openocd, given by "$ which openocd" :

.. code-block:: console

   $ west flash --openocd /usr/local/bin/openocd

Flashing an application to Nucleo G431KB
----------------------------------------

Connect the Nucleo G431KB to your host computer using the USB port,
then run a serial host program to connect with your Nucleo board.

.. code-block:: console

   $ minicom -D /dev/ttyACM0

Now build and flash an application. Here is an example for
:ref:`hello_world`.

.. zephyr-app-commands::
   :zephyr-app: samples/hello_world
   :board: nucleo_g431kb
   :goals: build flash

You should see the following message on the console:

.. code-block:: console

   $ Hello World! arm


Debugging
=========

You can debug an application in the usual way.  Here is an example for the
:ref:`hello_world` application.

.. zephyr-app-commands::
   :zephyr-app: samples/hello_world
   :board: nucleo_g431kb
   :maybe-skip-config:
   :goals: debug

.. _Nucleo G431KB website:
   https://www.st.com/en/evaluation-tools/nucleo-g431kb.html

.. _STM32G4 Nucleo-32 board User Manual:
   https://www.st.com/resource/en/user_manual/um2397-stm32g4-nucleo32-board-mb1430-stmicroelectronics.pdf

.. _STM32G431KB on www.st.com:
   https://www.st.com/en/microcontrollers/stm32g431kb.html

.. _STM32G4 reference manual:
   https://www.st.com/resource/en/reference_manual/rm0440-stm32g4-series-advanced-armbased-32bit-mcus-stmicroelectronics.pdf
